Vision
======

New team members to Matrix AI should get acquainted with the vision and philosophy of Matrix AI.

The implications of the Matrix extend beyond just PAAS or IAAS or OAAS. This document presents some visions with regards to the ubiquity of the Matrix system.

Resources:
----------

* http://blogs.wsj.com/accelerators/2014/10/10/weekend-read-the-imminent-decentralized-computing-revolution/
* Microservices vs Monolithic Apps - http://highscalability.com/blog/2014/7/28/the-great-microservices-vs-monolithic-apps-twitter-melee.html
* Microservice Issues - https://news.ycombinator.com/item?id=8227721
* Describing Orchestration: http://garyberger.net/?p=674
* Problems with existing CM: https://news.ycombinator.com/item?id=8324239
* Physical Web from Google: https://github.com/google/physical-web/blob/master/README.md
* http://m.fastcompany.com/3031272/can-jeff-immelt-really-make-the-world-1-better

Physical Event Data
-------------------

The Matrix is in the perfect position to take advantage of the IoT movement. The big companies of these days are likes of Google, Twitter, Facebook.. etc. These companies overwhelming took advantage of digital event data.

The next big thing is physical event data.

When you merge the physical with the digital, you can acquire all physical event data, which allows the possibility for a physical event data search engine.

This would be bigger than any of the current data base web services, which can only search through web data, because digital data is already digitised.

Physical event data search engines would allow one to ask questions like "How many people went into walmart on Tuesday 3pm to 4pm 5th of January 2016"? Physical event data's monetisation strategy is not just advertising. But it is decision making of individuals and businesses. People and businesses will be able to make informed decisions based on realtime physical event data.

Previously this was limited for a couple reasons:

1. Sensors and embedded devices were not commoditised, they were proprietary and specialised and expensive.
2. The software stack in these devices were not standardised, commoditised or compatible with open source development models. Meaning often the hardware vendor IS ALSO the software vendor. These hardware are not "platforms" but their just proprietary tools.
3. Access/Cultural norms. People aren't used to sensors or embedded devices everywhere.

These factors are changing.

We have now the first generation of simple cheap commoditised open hardware such as Arduino and Raspberry Pi. Most people and businesses do not understand these devices. The only people playing with these are hobbyists and hardware hackers. This mirrors the PC industry when it was just beginning. When PCs first began, what came before were large proprietary mainframes, the first PCs were nothing more than just toys for geeks. When PCs grew, they first grew as independent walled gardens, which each hardware vendor being the software vendor. If you bought a Solaris, you also bought the Solaris software ... etc. Microsoft came along and commoditised its complements, but making a hardware vendor neutral operating system. This allowed Microsoft to capture most of the value and reduces hardware's differentiating factor. Microsoft then became a "platform".

The same will happen with embedded devices. There will be a player who can abstract over all embedded devices, and become the platform "operating system" for embedded devices. However the number of embedded devices will explode. The number devices that will exist in the IoT space will be greater than any of us can imagine. There'll be devices in our clothes, our chairs, our cars, our tables, our cutlery, our glasses, our food... etc.

The management or scaling of these devices is an orchestration problem. When PCs came out, software was built as a contained system for the use at one single workstation. Eventually we moved to the cloud, which brings benefits but also increasing complexities of orchestration.

The Matrix is the operating system of the future. It is more than just a data center operating system. It is the internet of things operating system. It will perform the orchestration of millions/billions of embedded devices.

Creating OAAS for VM to provide a powerful PAAS for web services is only step 1. This is a potentially a 100 million dollar market. Creating an OS for the IoT is a billion dollar market, is our step 2.

We're thinking too small when it comes to distributed systems. If you are an enterprise with 100s or 1000s of servers, this is nothing compared to millions of embedded devices that need to be orchestrated. One example is OTA upgrades, such as when Tesla maintained their car firmware over the air. The world is a distributed system. It brings along benefits such as smart grids, home automation, smart cities, and smart countries. The world would be the biggest distributed system ever made, and Matrix will be there to operate it.

Who would be competing against in this space? Our current competitors in the PAAS market are thinking too small. When we get to the IoC space, the true competitor will be Google. Physical event data is far more valuable than web pages. Google is thinking about this space too, their acquisitions in Nest, the production of Google glass, the deployment of Google fibre and the invention of self driving cars show that Google is realising where the digital can meet the physical world.

When Matrix acts as the operating system of the world, then it is in a prime position to record and analyse physical event data. The knowledge and skills of understanding and operating a distributed system is very different from making hardware embedded devices. This allows us to work together with hardware vendors.

See this, GE is looking into this: http://m.fastcompany.com/3031272/can-jeff-immelt-really-make-the-world-1-better

Just think a 1% improvement in productivity in large industries is worth billions. Look at how sensors can help maintenances on industrial equipment. Sensors on small planes can reduce maintanence time and focus maintenance engineers on fixing what's important.

http://en.wikipedia.org/wiki/List_of_sensors

IOT
---

Microcontroller systems:

* Contiki
* Spark.io
* Helium.co
* Zigbee