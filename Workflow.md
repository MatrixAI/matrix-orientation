# Workflow

In order to get into the workflow of the Matrix AI team, make sure to behave in these ways:

## Email

* Receive invitation for a matrix.ai email.
* Use this email for all official correspondence.
* Use this email for when publishing intellectual property (including software and content).
* Use this email for internal communication or sending or receiving invoices.

## Phone

* Pass your mobile phone number to all other teammates so they contact you in emergencies.

## Chat

* Receive invitation to Matrix AI Slack.
* Receive invitation to Matrix AI Gitter.
* Make sure to download the Slack application on to your mobile phone.
* Use Slack for private company chat.
* Use Gitter for public chat with the open source community.

## Video/Call Conference

* We generally use Skype for video calls.

## Content Publishing

* Receive invitation to matrix.ai blog.
* You may be expected to write a short article for publishing.

## Revision Control

* Receive invitation to Github Organisation (provide Username if applicable).
* Receive invitation to Bitbucket Organisation (provide username if applicable).
* Commit early and often to reduce merge conflicts, we need to converge rapidly.
* We use source control to share files as well such as small binaries. But for bigger binaries, use Amazon S3.
* Only for temporary file transfer do we use Dropbox. Anything persistent should either be in the source control or in Amazon S3.

## Time Tracking

* Receive invitation to Toggl.
* You must track your time for all Matrix activities.
* Your time must be tagged with one or more of these:
    * Administration
    * Communication
    * Development
    * Event
    * Marketing
    * Meeting (External)
    * Meeting (Internal)
    * Planning
    * Research
    * Transportation
* You need succinctly describe your time used for any particular activity.
* As a contractor or casual employee, you'll be required to submit a monthly time report.

## Project and Issue Management

* Use issues on Github projects to discuss plans and directions.
* Commit notes into repositories that you are working on.
* Use Github Project boards to keep track of overall projects.

## Calendar

* Receive invitation to the Matrix AI calendar.
