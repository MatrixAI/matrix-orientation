# Authorship

Where the author of software or other intellectual property is required, Matrix AI team members should use their online alias if it is relevant to an online community, real name if it is relevant to meatspace. The contact email should always be your Matrix AI email. This will be relevant for any maintainer records for open source software.
