# Naming

Projects are end-user products. Or they represent a consolidation of several components.  Projects should be named with capitals. For example `Architect` is a project.

Libraries should be named with `lang-name` format. The `lang` should be similar to the language extension: `c`, `js`, `hs`. The `name` should be a lowercase name that should be the same name as the name of the package that other developers can reference. For example `js-virtualfs` is a library. The name of the package is actually `virtualfs`. This is sometimes not always feasible, because a name is already taken, or that we have forked from another project. In that case we may in fact use the full name `lang-name` as the package name if we are not merging back into the upstream library.
