# Research

In the development of Matrix AI, you will be required to do research.

Research is done in several phases:

* Review of Resources - You may be given a list of links and resources to review. You're not expected to read everything, but think of it as a starting point and as a reference manual.
* Writing Notes - When approaching a new project or an existing project, you will need to write down notes based on the research and how to apply it to the project you are tasked on. These notes should be committed in the project repository. If the project is open source, you can keep in the open source project repository. If it is not, you will need to keep it in the proprietary repository. These notes are essential part of our R&D process and represents evidence of research.
* Consolidation - Once the project starts in ernest, this usually means you have a better idea of what you are doing and how you will do it. At this point or some time later, you will need to consolidate your research into a publishable content. Depending on the situation you may write this as a blog post on matrix.ai, or as part of design documents in the project repository. Remember that while notes represents a stream of conciousness, a design document can be used by another developer to fully implement the solution. This means design documents need to be clear to other people, and will be continuously iterated upon.
