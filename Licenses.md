# Licenses

Matrix AI open source software should be released using the Apache Public License 2.0 unless otherwise specified. The reason why we prefer Apache Public License 2.0 is because of both its trademark protection and patent clause.

The code for this license in many package managers is `Apache-2.0`.

Where copyright notices are relevant, they need to be written as `(c) Matrix AI YYYY`, where `YYYY` is the year that the project was started.

Make sure not to link to any GPL licensed library.

If you have forked an open source project, and you don't intend to merge back to upstream. Our derivative work should be licensed under Apache Public License 2.0. You have to preserve the original author's license and copyright notice however. This usually works by our copyright notice after any existing copyright notice.
