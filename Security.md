# Security

Matrix AI practices proper security practices for all of its operations.

This means you are expected to generate an SSH key and GPG key for Matrix operations.

Your Github and Bitbucket account should already have your own private SSH or GPG key. This is what you will normally use when working on public opensource Matrix projects.

However when working on Matrix equipment, you should have a generated a Matrix specific SSH key, and only use those keys when on Matrix equipment. With GPG, you can create subkeys for this purpose. The reason you should do this is because you don't control the equipment, thus you should not be keeping your personal private keys on it.

Make sure to submit public contact information in the Matrix Team repository including all public keys.
